package com.example.codereviewapi.repository;

import com.example.codereviewapi.model.PullRequest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.codereviewapi.utils.Constants.TEST_DESCRIPTION;
import static com.example.codereviewapi.utils.Constants.TEST_STATUS;
import static com.example.codereviewapi.utils.Constants.TEST_TITLE;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(MockitoExtension.class)
@DisplayName("Pull Request Repository Test")
public class PullRequestRepositoryTest {

    @InjectMocks
    private PullRequestRepository pullRequestRepository;

    @Test
    @DisplayName("Save Pull Request Should return an id which corresponds to the numbers of pull request in the list")
    void saveShouldReturnAnIdWhichCorrespondToNumberOfItemsInList() {
        PullRequest pullRequest = PullRequest.builder().title(TEST_TITLE).description(TEST_DESCRIPTION).status(TEST_STATUS).build();

        assertThat(pullRequestRepository.save(pullRequest)).isEqualTo(1);
    }
}
