package com.example.codereviewapi.controller;

import com.example.codereviewapi.service.PullRequestService;
import org.dozer.Mapper;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.example.codereviewapi.utils.Constants.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PullRequestController.class)
@DisplayName("Pull Request Controller Test")
public class PullRequestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private Mapper mapper;

    @MockBean
    private PullRequestService pullRequestService;

    @Test
    @DisplayName("When all required fields are present the endpoint should return 200 and id.")
    void postAddPullRequestShouldReturnAnIdWhenPullRequestAdded() throws Exception {
        when(pullRequestService.addPullRequest(any())).thenReturn(1);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", TEST_TITLE);
        jsonObject.put("description", TEST_DESCRIPTION);
        jsonObject.put("status", TEST_STATUS);

        mockMvc.perform(post(PULL_REQUEST_URL).contentType(MediaType.APPLICATION_JSON).content(jsonObject.toString()))
                .andExpect(status().isOk()).andExpect(jsonPath("$.id", Matchers.is(TEST_ID)));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @DisplayName("When title is null or empty the endpoint should return a bad request status")
    void postAddPullRequestShouldReturnBadRequestWhenTitleIsNullOrEmpty(String title) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", title);
        jsonObject.put("description", TEST_DESCRIPTION);
        jsonObject.put("status", TEST_STATUS);

        mockMvc.perform(post(PULL_REQUEST_URL).contentType(MediaType.APPLICATION_JSON).content(jsonObject.toString()))
                .andExpect(status().isBadRequest());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @DisplayName("When description is null or empty the endpoint should return a bad request status")
    void postAddPullRequestShouldReturnBadRequestWhenDescriptionIsNullOrEmpty(String description) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", TEST_TITLE);
        jsonObject.put("description", description);
        jsonObject.put("status", TEST_STATUS);

        mockMvc.perform(post(PULL_REQUEST_URL).contentType(MediaType.APPLICATION_JSON).content(jsonObject.toString()))
                .andExpect(status().isBadRequest());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @DisplayName("When status is null or empty the endpoint should return a bad request status")
    void postAddPullRequestShouldReturnBadRequestWhenStatusIsNullOrEmpty(String status) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("title", TEST_TITLE);
        jsonObject.put("description", TEST_DESCRIPTION);
        jsonObject.put("status", status);

        mockMvc.perform(post(PULL_REQUEST_URL).contentType(MediaType.APPLICATION_JSON).content(jsonObject.toString()))
                .andExpect(status().isBadRequest());
    }
}
