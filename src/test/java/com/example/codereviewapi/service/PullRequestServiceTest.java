package com.example.codereviewapi.service;

import com.example.codereviewapi.model.PullRequest;
import com.example.codereviewapi.repository.PullRequestRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.codereviewapi.utils.Constants.TEST_DESCRIPTION;
import static com.example.codereviewapi.utils.Constants.TEST_STATUS;
import static com.example.codereviewapi.utils.Constants.TEST_TITLE;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@DisplayName("Pull Request Service Test")
public class PullRequestServiceTest {

    @InjectMocks
    private PullRequestService pullRequestService;

    @Mock
    private PullRequestRepository pullRequestRepository;

    @Test
    @DisplayName("Test that a pull request is added to the repository and an id is returned")
    public void addPullRequestShouldReturnAnId() {
        PullRequest pullRequest = PullRequest.builder().title(TEST_TITLE).description(TEST_DESCRIPTION).status(TEST_STATUS).build();
        given(pullRequestRepository.save(any())).willReturn(1);

        int id = pullRequestService.addPullRequest(pullRequest);
        assertThat(id).isEqualTo(1);

        ArgumentCaptor<PullRequest> pullRequestArgumentCaptor = ArgumentCaptor.forClass(PullRequest.class);

        verify(pullRequestRepository).save(pullRequestArgumentCaptor.capture());

        PullRequest repoPullRequest = pullRequestArgumentCaptor.getValue();

        assertThat(repoPullRequest).usingRecursiveComparison().isEqualTo(pullRequest);
    }
}
