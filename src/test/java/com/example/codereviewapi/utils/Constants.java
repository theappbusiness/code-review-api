package com.example.codereviewapi.utils;

public class Constants {
    public static final String PULL_REQUEST_URL = "/pull-request";
    public static final String TEST_TITLE = "dummy title";
    public static final String TEST_DESCRIPTION = "dummy description";
    public static final String TEST_STATUS = "Passed";
    public static final int TEST_ID = 1;
}
