package com.example.codereviewapi.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddPullRequestDto {

    @NotEmpty
    private String title;

    @NotEmpty
    private String description;

    @NotEmpty
    private String status;

}
