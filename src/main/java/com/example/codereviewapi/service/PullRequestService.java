package com.example.codereviewapi.service;

import com.example.codereviewapi.model.PullRequest;
import com.example.codereviewapi.repository.PullRequestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PullRequestService {

   private final PullRequestRepository pullRequestRepository;

   public int addPullRequest(PullRequest pullRequest) {
      return pullRequestRepository.save(pullRequest);
   }
}
