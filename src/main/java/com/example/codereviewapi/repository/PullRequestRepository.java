package com.example.codereviewapi.repository;

import com.example.codereviewapi.model.PullRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PullRequestRepository {

    private List<PullRequest> pullRequestList = new ArrayList<>();

    public int save(PullRequest pullRequest) {
        pullRequest.setId(pullRequestList.size() + 1);
        pullRequestList.add(pullRequest);
        return pullRequest.getId();
    }
}
