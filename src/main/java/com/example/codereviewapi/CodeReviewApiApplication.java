package com.example.codereviewapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodeReviewApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CodeReviewApiApplication.class, args);
    }

}
