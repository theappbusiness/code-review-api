package com.example.codereviewapi.controller;

import com.example.codereviewapi.model.PullRequest;
import com.example.codereviewapi.model.dto.AddPullRequestDto;
import com.example.codereviewapi.model.dto.PullRequestAddedDto;
import com.example.codereviewapi.service.PullRequestService;
import lombok.RequiredArgsConstructor;
import org.dozer.Mapper;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "pull-request")
public class PullRequestController {
    private final PullRequestService pullRequestService;
    private final Mapper mapper;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PullRequestAddedDto> addPullRequest(@Validated @RequestBody AddPullRequestDto addPullRequestDto) {
        PullRequest pullRequest = mapper.map(addPullRequestDto, PullRequest.class);
        int id = pullRequestService.addPullRequest(pullRequest);
        return ResponseEntity.ok(new PullRequestAddedDto(id));
    }
}
