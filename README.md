# Code Review API

### Description
This API will allow us to manage Pull Requests.

## Getting started
### Prerequisites

- ≥ Java 11
- Maven is the dependency manager utilised


### Endpoints

#### Add Pull Request

```bash
POST /pull-request
```
### Running the app
Clone the repository:
```shell 
$ git clone git@bitbucket.org:theappbusiness/code-review-api.git
```

Change directory into the application root and run the application.

```shell
$ cd code-review-api
$ mvn spring-boot:run
```

To run the tests:

```shell
$ mvn test
```

## Styling
### Branch Names
Follow this naming convention for branches:

    <Type>/<ticket-id>-what-the-change-does     
    - Type: chore, docs, feat, fix, refactor, style, or test.


### Commits
Commit message format in accordance with https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716

    <type>(<scope>): <subject>
    - Summary in present tense.


